<?php

	require APPPATH.'/libraries/REST_Controller.php';
	class ReportController extends REST_Controller{

		function reports_get(){
			$this->load->model('reportsModel');
        	$users = $this->reportsModel->get_all(); 
        	if($users){
            	$this->response($users, 200);
        	}
        	else{
            	$this->response(NULL, 404);
        	}
    	}

        function report_get(){
            $id = $this->get('id');
            $this->load->model('reportsModel');
            $report = $this->reportsModel->get_one($id);
            if($report){
                $this->response($report, 200);
            }
            else{
                $this->response(NULL, 404);
            }
        }

        function report_post(){
            $report['id'] = $this->post('id');
            $report['coordinates'] = $this->post('coordinates');
            $report['username'] = $this->post('username');
            $report['description'] = $this->post('description');
            $this->load->model('reportsModel');
            $this->reportsModel->post($report);
            $report['message'] = 'Created';
            $this->response($report['message'], 200);
        }
	}

?>