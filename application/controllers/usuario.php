<?php

	require APPPATH.'/libraries/REST_Controller.php';

	class Usuario extends REST_Controller{

		function users_get(){
			$this->load->model('usuariosmodel');
        	$users = $this->usuariosmodel->get_all(); 
        	if($users){
            	$this->response($users, 200);
        	}
        	else{
            	$this->response(NULL, 404);
        	}
    	}

        function user_get(){
            $username = $this->get('username');
            $this->load->model('usuariosmodel');
            $user = $this->usuariosmodel->get_one($username);
            if($user){
                $this->response($user, 200);
            }
            else{
                $this->response(NULL, 404);
            }
        }

        function user_post(){
            $user['oldUsername'] = $this->post('oldUsername');
            $user['newUsername'] = $this->post('newUsername');
            $user['email'] = $this->post('email');
            $this->load->model('usuariosmodel');
            $this->usuariosmodel->update($user);
            $user['message'] = 'Added';
            $this->response($user['message'], 200);
        }

        function user_put(){
            $user['username'] = $this->put('username');
            $user['email'] = $this->put('email');
            $user['password'] = $this->put('password');
            $this->load->model('usuariosmodel');
            $this->usuariosmodel->put($user);
            $user['message'] = 'Created';
            $this->response($user['message'], 200);
        }

        function user_delete(){
            $this->load->model('usuariosmodel');
            $this->usuariosmodel->delete($this->get('username'));
            $message = array('username' => $this->get('username'), 'message' => 'DELETED');
            $this->response($message, 200);
        }

	}

?>