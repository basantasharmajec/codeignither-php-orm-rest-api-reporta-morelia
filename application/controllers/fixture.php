<?php
    if(!defined('BASEPATH'))
        exit('No direct script access allowed');
    class Fixture extends CI_Controller {

        function Fixture(){
            parent::__Construct();
            $this->load->model('usuariosmodel');
        }

        function index() {
            if(ENVIRONMENT != 'development')
                return;
            $this->_user();
        }
        
        function _user() {
            $users = array(
                array(
                    'username' => 'jjjulio',
                    'email' => 'julio.medina.9@gmail.com',
                    'password' => 'contrasenaPerronsisima007'
                ),
                array(
                    'username' => 'bbbravo',
                    'email' => 'eduardo.bravo@gmail.com',
                    'password' => 'noSoyUnaContrasena'
                ),
                array(
                    'username' => 'cccristian',
                    'email' => 'cristian.capilla@gmail.com',
                    'password' => 'contrasenafea'
                )
            );
            $this->usuariosmodel->insertBatch($users);
        }

    }
?>