<?php

class Migration_Users extends CI_Migration{
	public function up(){
		$fields = array(
			'id' => array(
					'type' => 'INT',
					'constraint' => 11,
					'null' => FALSE,
					'auto_increment' => TRUE
				),
			'username' => array(
					'type' => 'VARCHAR',
					'constraint' => 20,
					'null' => FALSE
				),
			'email' => array(
					'type' => 'VARCHAR',
					'constraint' => 30,
					'null' => FALSE
				),
			'password' => array(
					'type' => 'VARCHAR',
					'constraint' => 30,
					'null' => FALSE
				)
			);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);

		$this->dbforge->create_table('usuarios', TRUE);
	}

	public function down(){
		$this->dbforge->drop_table('usuarios');
	}
}
?>